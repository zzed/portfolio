import { TestBed } from '@angular/core/testing';

import { ScrollProgressService } from './scroll-progress.service';

describe('ScrollProgressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScrollProgressService = TestBed.get(ScrollProgressService);
    expect(service).toBeTruthy();
  });
});
