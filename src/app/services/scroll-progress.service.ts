import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScrollProgressService {
  private current = new BehaviorSubject('');
  private pages = [
    {
      name: 'landing',
      title: 'Home'
    },
    {
      name: 'about',
      title: 'About me'
    },
    {
      name: 'what-i-made',
      title: 'What I made'
    },
    {
      name: 'mr-decal',
      title: 'Mr. Decal'
    }
  ];
  constructor() {}
  newPoint(x: string) {
    this.current.next(x);
  }
  get availablePages() {
    return this.pages;
  }
  get currentPoint() {
    return this.current.asObservable() as Observable<string>;
  }
}
