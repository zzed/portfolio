import { trigger, state, style, transition, animate } from '@angular/animations';

export const fadeIntoView = trigger('inView', [
  state('inView', style({
    opacity: 1,
    transform: 'translateY(0)'
  })),
  state('outView', style({
    opacity: 0,
    transform: 'translateY(20px)'
  })),
  transition('outView <=> inView', [
    animate('.5s .5s')
  ]),
  transition('inView <=> outView', [
    animate('.2s')
  ])
]);
