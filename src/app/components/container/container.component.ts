import { Component, OnInit, Input } from '@angular/core';
import { fadeIntoView } from 'src/app/fadeIntoView';
import { ScrollProgressService } from 'src/app/services/scroll-progress.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
  animations: [
    fadeIntoView
  ]
})
export class ContainerComponent implements OnInit {

  @Input() position: string;
  @Input() id: string;
  inView = false;

  constructor(private sP: ScrollProgressService) { }

  ngOnInit() {
  }

  public onIntersection({ visible }: { visible: boolean }): void {
    if (visible) {
      this.inView = true;
      this.sP.newPoint(this.id);
    } else if (!visible) {
      this.inView = false;
    }
  }
}
