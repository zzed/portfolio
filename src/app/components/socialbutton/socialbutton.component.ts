import { Component, OnInit, Input } from '@angular/core';
import { faTwitter, IconDefinition, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faWindowMaximize } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-socialbutton',
  templateUrl: './socialbutton.component.html',
  styleUrls: ['./socialbutton.component.scss']
})
export class SocialbuttonComponent implements OnInit {

  @Input() social: string;
  @Input() address: string;
  icon: IconDefinition;

  constructor() { }

  ngOnInit() {
    switch (this.social) {
      case 'Twitter':
        this.icon = faTwitter;
        break;
      case 'Gitlab':
        this.icon = faGitlab;
        break;
      default:
        this.icon = faWindowMaximize;
        break;
    }
  }

}
