import { Component, OnInit } from '@angular/core';
import { ScrollProgressService } from 'src/app/services/scroll-progress.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})

export class ProgressComponent implements OnInit {

  current: Observable<string>;
  pages = this.sP.availablePages;
  constructor(private sP: ScrollProgressService) {}

  ngOnInit() {
    this.current = this.sP.currentPoint;
  }

}
